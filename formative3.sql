/*
> Formative Task 3
*/
use staging;
insert into Product_ClientA(`name`, `description`, artNumber, price, stock, `delete`, brand_name) values 
('BINTANG ILM 500G', 'Olahan ikan bentuk bintang, berat 500 gram', '1234123400001', 11500, 80, false, 'ILM'),
('SUKOI ILM 500G', 'Olahan ikan bentuk lingkaran, berat 500 gram', '1234123400002', 10500, 50, false, 'ILM'),
('SCALLOP ILM 500G', 'Olahan ikan dengan sayur bentuk lingkaran, berat 500 gram', '1234123400003', 12500, 40, false, 'ILM'),
('TEMPURA ILM 500G', 'Olahan ikan bentuk tempura, berat 500 gram', '1234123400004', 10500, 14, false, 'ILM'),
('BURGER ILM 500G', 'Olahan ikan bentuk burger, berat 500 gram', '1234123400005', 12000, 30, false, 'ILM'),
('TAHU IKAN', 'Olahan ikan premium bentuk tahu, berat 450 gram', 1234123400006, 23000, 5, false, 'ILM'),
('NUGGET ICE CREAM', 'Nugget ikan bentuk es krim, isi 24', '1234123400007', 17500, 8, false, 'SAKURA'),
('BOLA IKAN', 'Olahan ikan premium, bentuk bakso, berat 500 gram', '1234123400008', 5, 21000, false, 'MINAKU'),
('BOLA UDANG', 'Olahan udang, bentuk bakso, berat 500 gram', '1234123400009', 8, 25000, false, 'MINAKU'),
('SCALLOP MINAKU', 'Olahan ikan premium dengan sayur, bentuk lingkaran, berat 500 gram', '1234123400009', 15, 17500, false, 'MINAKU'),
('CRAB STICK', 'Olahan daging kepiting, bentuk mie, isi 12', '1234123400010', 10, 22500, false, 'CEDEA');

insert into Product_ClientB(nama, deskripsi, nomor_artikel, harga, persediaan_barang, dihapus, nama_merek) values 
('BINTANG MINAKU 500G', 'Olahan ikan premium bentuk bintang, berat 500 gram', '1234123400011', 17500, 10, false, 'MINAKU'),
('SUKOI MINAKU 500G', 'Olahan ikan premium bentuk lingkaran, berat 500 gram', '1234123400012', 16500, 10, false, 'MINAKU'),
('SCALLOP MINAKU 500G', 'Olahan ikan premium dengan sayur bentuk lingkaran, berat 500 gram', '1234123400013', 18500, 10, false, 'MINAKU'),
('TEMPURA MINAKU 500G', 'Olahan ikan premium bentuk tempura, berat 500 gram', '1234123400014', 15000, 20, false, 'MINAKU'),
('SOSIS NGETOP 15', 'Sosis ayam merah isi 15', '1234123400015', 17500, 15, false, 'NGETOP'),
('SOSIS NGETOP 30', 'Sosis ayam merah isi 30 + 2', '1234123400016', 25000, 55, false, 'NGETOP'),
('SOSIS UMAMI 30', 'Sosis ayam sapi kombinasi isi 30', '1234123400017', 21000, 20, false, 'NGETOP'),
('SOSIS SOBAT 30', 'Sosis ayam merek sobat isi 30', '1234123400018', 19000, 20, false, 'SOBAT'),
('KIMBO SOSIS SAPI BRATWURST 10', 'Sosis sapi besar isi 10', '1234123400019', 31000, 10, false, 'KIMBO'),
('KIMBO SOSIS SAPI BRATWURST 5', 'Sosis sapi besar isi 5 x 2', '1234123400019', 31000, 10, false, 'KIMBO');