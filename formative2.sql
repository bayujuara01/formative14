/*
> Formative Task 2
*/
use staging;
create table Product_ClientA (
	id int primary key auto_increment,
    `name` varchar(60),
    `description` text,
    artNumber varchar(40),
    price int not null,
    stock int not null default 0,
    `delete` boolean,
    brand_name varchar(60) not null
);

create table Product_ClientB (
	id int primary key auto_increment,
    nama varchar(60),
    deskripsi text,
    nomor_artikel varchar(40),
    harga int not null,
    persediaan_barang int not null default 0,
    dihapus boolean,
    nama_merek varchar(60) not null
);

use production;
create table Product (
	id int primary key auto_increment,
    `name` varchar(60),
    `description` text,
    art_number varchar(40),
    price int not null,
    stock int not null default 0,
    is_delete boolean,
    brand varchar(60) not null
);