/*
> Bayu Seno Ariefyanto
> Formative Task 6
> My Solution : 
1. Merubah nama kolom dari brand ke brand_id
2. Update setiap row tabel product yaitu column brand dengan id-
yang terdapat pada tabel brand dan disesuaikan dengan namanya, dapat dilakukan dengan JOIN kedua tabel
3. Alter table brand_id ke tipe integer
4. tambahkan foreign key ke table brand
*/

USE production;
ALTER TABLE product RENAME COLUMN brand TO brand_id;

UPDATE product JOIN brand ON product.brand_id = brand.`name`
	SET product.brand_id = brand.id;

ALTER TABLE product MODIFY brand_id INT NOT NULL;

ALTER TABLE product ADD CONSTRAINT FK_Brand_Product FOREIGN KEY (brand_id) REFERENCES brand(id);    

SELECT product.*, brand.name FROM product JOIN brand ON product.brand_id = brand.id;