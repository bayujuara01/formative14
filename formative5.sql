/*
> Formative Task 5
*/
use production;
create table if not exists brand (
	id int primary key auto_increment,
    `name` varchar(40) not null
);

insert into brand(`name`) values 
('ILM'), 
('MINAKU'), 
('SAKURA'), 
('CEDEA'), 
('KIMBO'), 
('SOBAT'), 
('NGETOP');