/*
> Bayu Seno Ariefyanto
> Formative Task 4
> My Solution : 
1. Melakukan insert yang digabungkan dengan select dari tabel database lain
2. Disesuaikan peletakan kolomnya harus setara jika name, harus sama walau nama berbeda
3. Ulangi untuk tabel kedua
*/
INSERT INTO production.product(`name`, `description`, art_number, price, stock, is_delete, brand)
	SELECT `name`, `description`, artNumber, price, stock, `delete`, brand_name FROM staging.product_clienta;

INSERT INTO production.product(`name`, `description`, art_number, price, stock, is_delete, brand)
	SELECT nama, deskripsi, nomor_artikel, harga, persediaan_barang, dihapus, nama_merek FROM staging.product_clientb;    
